package ru.tfs.reactive.functional;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.web.reactive.function.server.ServerResponse.ok;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RequestPredicates;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;
import ru.tfs.reactive.domain.Car;
import ru.tfs.reactive.repository.CarsRepository;

@Configuration
public class ReactiveFunctionalConfiguration {

    private static final String URL = "/api/v2/cars";

    @Bean
    public RouterFunction<ServerResponse> routes(CarsRepository repository) {

        return RouterFunctions.route()
            .GET(URL,
                 RequestPredicates.accept(APPLICATION_JSON),
                 serverRequest -> ok().contentType(APPLICATION_JSON).body(repository.findAll(), Car.class)
            )
            .GET(URL + "/{id}",
                 RequestPredicates.accept(APPLICATION_JSON),
                 serverRequest -> {
                     long id = Long.parseLong(serverRequest.pathVariable("id"));
                     return ok().contentType(APPLICATION_JSON)
                         .body(repository.findById(id), Car.class);
                 }
            )
            .POST(URL,
                  RequestPredicates.accept(APPLICATION_JSON),
                  serverRequest -> serverRequest
                      .bodyToMono(Car.class)
                      .map(repository::save)
                      .flatMap(car -> ok().body(car, Car.class))
            )
            .build();
    }
}
